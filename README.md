# Varbase Email

Adds HTML email templates for Drupal.

## [Varbase documentation](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/core-components/varbase-email)
Check out Varbase documentation for more details.

* [Varbase Email Module](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/core-components/varbase-email)
* [Configure Symfony Mailer](https://docs.varbase.vardot.com/v/10.0.x/developers/configuring-a-varbase-site/configuring-varbase-mailer-settings/configure-symfony-mailer)

Join Our Slack Team for Feedback and Support
http://slack.varbase.vardot.com/

This module is sponsored and developed by [Vardot](https://www.drupal.org/vardot).
